PACKAGES = fuse glib-2.0
CFLAGS  += $(shell pkg-config --cflags $(PACKAGES)) -std=c11 -Wall -pedantic
LDFLAGS += $(shell pkg-config --libs $(PACKAGES))

default: gen fuse-ramdisk-fs

gen:
	../../github/litmus/bin/litmus fuse-ramdisk-fs.lit.md -u -g
