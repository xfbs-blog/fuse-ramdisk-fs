#define FUSE_USE_VERSION 31
#include <fuse.h>
#include <stdlib.h>
#include <stdbool.h>
#include <sys/stat.h>
#include <string.h>
#include <errno.h>
#include <gmodule.h>
#include <assert.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>
#ifndef NDEBUG
#include <stdio.h>
#define debug(...) fprintf(stderr, __VA_ARGS__)
#define zero(ptr) assert(ptr); memset(ptr, 0, sizeof(*ptr))
#else
#define debug(...)
#define zero(ptr) assert(ptr)
#endif

const size_t blocksize = 4098;

typedef struct {
  mode_t mode;
  uid_t uid;
  gid_t gid;

  union {
    struct {
      GHashTable *entries;
    } dir;
    struct {
      nlink_t links;
      size_t size;
      GPtrArray *content;
    } file;
    struct {
      const char *target;
      size_t size;
    } link;
    struct {
      dev_t device;
    } block, character;
    struct {
      size_t _empty;
    } fifo, socket;
  };

  struct {
    struct timespec created, modified, accessed;
  } time;
} ramdisk_node;

typedef struct {
  ramdisk_node root;
} ramdisk_fs;

//! Returns a pointer to the parents node of the file pointed to by `path`.
//! Guaranteed to be a dir node. Returns `NULL` on error, puts error code in
//! `errno`.
static inline ramdisk_node *
ramdisk_node_parent(struct fuse_context *ctx, const char *path, const char **entry)
{
  if(!ctx) ctx = fuse_get_context();  assert(ctx);
  ramdisk_fs *fs = ctx->private_data; assert(fs);

  // "/path/to/file" => ["path", "to", "file", NULL]
  gchar **path_components = g_strsplit(&path[1], "/", 0);
  assert(path_components);

  ramdisk_node *node = &fs->root;
  for(size_t pos = 0; path_components[pos]; ++pos) {
    // if this is the last path component, save it in entry and exit loop.
    if(!path_components[pos + 1]) {
      *entry = path_components[pos];
      break;
    }

    // even if something goes wrong while parsing the dir, we'll still need to
    // free all the path components (up to the last).
    if(!node) {
      g_free(path_components[pos]);
      continue;
    }

    // make sure we have a dir.
    if(!S_ISDIR(node->mode)) {
      node = NULL;
      errno = ENOTDIR;
      continue;
    }

    // make sure that dir has some entries.
    if(!node->dir.entries) {
      node = NULL;
      errno = ENOENT;
      continue;
    }

    // make sure that dir has the entry we're looking for.
    node = g_hash_table_lookup(node->dir.entries, path_components[pos]);
    if(!node) {
      errno = ENOENT;
      continue;
    }
    
    g_free(path_components[pos]);
  }

  // make sure we have a dir.
  if(!S_ISDIR(node->mode)) {
    node = NULL;
    errno = ENOTDIR;
  }

  g_free(path_components);
  return node;
}

static ramdisk_node *
ramdisk_node_entry(struct fuse_context *ctx, const char *path)
{
  if(!ctx) ctx = fuse_get_context();  assert(ctx);
  ramdisk_fs *fs = ctx->private_data; assert(fs);

  // "/path/to/file" => ["path", "to", "file", NULL]
  gchar **path_components = g_strsplit(&path[1], "/", 0);
  assert(path_components);
  
  ramdisk_node *node = &fs->root;
  for(size_t pos = 0; path_components[pos]; ++pos) {
    // make sure we have a dir.
    if(!S_ISDIR(node->mode)) {
      node = NULL;
      errno = ENOTDIR;
      break;
    }

    // make sure that dir has some entries.
    if(!node->dir.entries) {
      node = NULL;
      errno = ENOENT;
      break;
    }

    // make sure that dir has the entry we're looking for.
    node = g_hash_table_lookup(node->dir.entries, path_components[pos]);
    if(!node) {
      errno = ENOENT;
      break;
    }
  }

  g_strfreev(path_components);
  return node;
}

void ramdisk_node_decref(gpointer data)
{
  assert(data);
  ramdisk_node *node = data;

  switch(node->mode & S_IFMT) {
  case S_IFBLK:
  case S_IFCHR:
  case S_IFIFO:
  case S_IFSOCK:
    free(node);
    break;
  case S_IFLNK: {
    const char *target = node->link.target;
    assert(target);
    g_free((gpointer) target);
    zero(node);
    free(node);
    }
    break;
  case S_IFDIR: {
    GHashTable *entries = node->dir.entries;
    assert(entries);
    g_hash_table_destroy(entries);
    zero(node);
    free(node);
    }
    break;
  case S_IFREG: {
    GPtrArray *content = node->file.content;
    assert(content);
    node->file.links--;
    if(!node->file.links) {
      g_ptr_array_unref(content);
      zero(node);
      free(node);
    }
    }
    break;
  default:
    debug("Error: illegal node type encountered (%hd)\n", node->mode);
    assert(false);
  }
}

static ramdisk_node *
ramdisk_node_new(ramdisk_node *node, mode_t mode, uid_t uid, gid_t gid, ...) {
  va_list opt;

  // make sure node points to something and that something is zeroed.
  if(!node) {
    node = calloc(1, sizeof(ramdisk_node));
    assert(node);
  } else {
    memset(node, 0, sizeof(*node));
  }

  // set common attributes
  node->mode = mode;
  node->uid = uid;
  node->gid = gid;

  va_start(opt, gid);
  switch(node->mode & S_IFMT) {
  case S_IFBLK:
  case S_IFCHR:
    {
      node->block.device = va_arg(opt, dev_t);
    }
    break;
  case S_IFLNK:
    {
      node->link.target = va_arg(opt, const char *);
      node->link.size = strlen(node->link.target);
    }
    break;
  case S_IFDIR:
  case S_IFREG:
  case S_IFIFO:
  case S_IFSOCK:
    break;
  default:
    if(mode) assert(false);
  }

  va_end(opt);

  // TODO: initialize time to something sensible

  debug("creating node at %p\n", (void *) node);
  return node;
}

static void *
ramdisk_init(struct fuse_conn_info *_conn)
{
  debug("init()\n");
  ramdisk_fs *fs = calloc(1, sizeof(ramdisk_fs));
  ramdisk_node_new(&fs->root, S_IFDIR | 0755, 0, 0);
  return fs;
}

static int
ramdisk_getattr(const char *path, struct stat *stat)
{
  debug("getattr(%s) = ", path);
  ramdisk_node *node = ramdisk_node_entry(NULL, path);
  if(!node) return -errno;

  // set common properties.
  memset(stat, 0, sizeof(*stat));
  stat->st_mode = node->mode;
  stat->st_ino = (ino_t) node;
  stat->st_uid = node->uid;
  stat->st_gid = node->gid;
  // stat->st_ctime = node->time.created;
  // stat->st_mtime = node->time.modified;
  // stat->st_atime = node->time.accessed;

  // set per-type properties.
  switch(node->mode & S_IFMT) {
  case S_IFBLK: stat->st_rdev = node->block.device;     break;
  case S_IFCHR: stat->st_rdev = node->character.device; break;
  case S_IFLNK: stat->st_size = node->link.size;        break;
  case S_IFDIR: break;
  case S_IFREG: stat->st_size = node->file.size;        break;
  case S_IFIFO: break;
  case S_IFSOCK: break;
  default: assert(false);
  }

  return 0;
}

static int
ramdisk_create(const char *path, mode_t mode, struct fuse_file_info *fi)
{
  const char *filename;
  struct fuse_context *ctx = fuse_get_context();
  ramdisk_node *node = ramdisk_node_parent(ctx, path, &filename);
  if(!node) return -errno;

/*
  // check if file already exists
  ramdisk_node *file = g_hash_table_lookup(node->children, filename);
  if(file) {
    debug("ENOENT\n");
    g_free(filename);
    return -ENOENT;
  }

  file = ramdisk_node_new(NULL, S_IFREG | mode, ctx->uid, ctx->gid);

  debug("success.\n");
  g_hash_table_insert(node->children, filename, ramdisk_node_file(NULL, mode));
  */
  return 0;
}

static int
ramdisk_readdir(const char *path, void *buf, fuse_fill_dir_t filler,
                off_t offset, struct fuse_file_info *fi)
{
  debug("readdir(%s)\n", path);
  ramdisk_node *node = ramdisk_node_entry(NULL, path);
  if(!node) return -errno;

/*
  if(!ramdisk_is_dir(node)) {
    debug("ENOENT\n");
    return -ENOENT;
  }
  assert(node->children);

  filler(buf, ".", NULL, 0);
  filler(buf, "..", NULL, 0);

  GHashTableIter iter;
  gpointer key, val;

  g_hash_table_iter_init(&iter, node->children);
  while(g_hash_table_iter_next(&iter, &key, &val)) {
    const char *item = key;
    debug("'%s', ", key);
    filler(buf, item, NULL, 0);
  }
*/
  return 0;
}

static int
ramdisk_mkdir(const char *path, mode_t mode)
{
/*
  debug("mkdir(%s) = ", path);

  struct fuse_context *ctx;
  ramdisk_fs *fs;
  ramdisk_node *node;
  gpointer dirname;
  ramdisk_get_ctx_fs_node(ctx, fs, node, path, &dirname);
  ramdisk_node *node = ramdisk_node_get(NULL, path, NULL);

  if(!ramdisk_is_dir(node)) {
    assert(!dirname);
    debug("ENOENT\n");
    return -ENOENT;
  }

  assert(node->children);
  assert(dirname);

  if(g_hash_table_lookup(node->children, dirname)) {
    debug("EPERM");
    g_free(dirname);
    return -EPERM;
  }

  g_hash_table_insert(node->children, dirname, ramdisk_node_dir(NULL, mode));
  
  debug("success.\n");
  */
  return 0;
}

static int
ramdisk_rmdir(const char *path)
{
/*
  debug("rmdir(%s) = ", path);

  struct fuse_context *ctx;
  ramdisk_fs *fs;
  ramdisk_node *node;
  gpointer dirname;
  ramdisk_get_ctx_fs_node(ctx, fs, node, path, &dirname);
  ramdisk_node *node = ramdisk_node_get(NULL, path, NULL);

  if(!ramdisk_is_dir(node)) {
    assert(!dirname);
    debug("ENOENT");
    return -ENOENT;
  }

  assert(node->children);
  assert(dirname);

  ramdisk_node *dir = g_hash_table_lookup(node->children, dirname);
  if(!ramdisk_is_dir(dir)) {
    debug("ENOENT\n");
    g_free(dirname);
    return -ENOENT;
  }

  debug("success.\n");
  g_hash_table_remove(node->children, dirname);
  g_free(dirname);
  */

  return 0;
}

static int
ramdisk_unlink(const char *path)
{
/*
  debug("unlink(%s) = ", path);

  struct fuse_context *ctx;
  ramdisk_fs *fs;
  ramdisk_node *node;
  gpointer filename;
  ramdisk_get_ctx_fs_node(ctx, fs, node, path, &filename);
  ramdisk_node *node = ramdisk_node_get(NULL, path, NULL);

  if(!ramdisk_is_dir(node)) {
    debug("ENOENT\n");
    g_free(filename);
    return -ENOENT;
  }

  ramdisk_node *file = g_hash_table_lookup(node->children, filename);
  if(!file) {
    debug("ENOENT\n");
    g_free(filename);
    return -ENOENT;
  }

  debug("success.\n");
  g_hash_table_remove(node->children, filename);
  */

  return 0;
}

static int
ramdisk_rename(const char *orig, const char *dest)
{
/*
  debug("rename(%s, %s) = ", orig, dest);
  struct fuse_context *ctx;
  ramdisk_fs *fs;
  ramdisk_node *orig_dir, *dest_dir;
  gpointer orig_file, dest_file;
  ramdisk_get_ctx_fs_node(ctx, fs, orig_dir, orig, &orig_file);
  dest_dir = ramdisk_node_get(&fs->root, dest, &dest_file);

  // make sure the parent dirs or orig and dest exist.
  if(!ramdisk_is_dir(orig_dir) || !ramdisk_is_dir(dest_dir)) {
    g_free(orig_file);
    g_free(dest_file);
    debug("ENOENT\n");
    return -ENOENT;
  }

  debug("orig_dir is %p, dest_dir is %p\n", orig_dir, dest_dir);

  // make sure that dest doesn't already exist.
  if(g_hash_table_lookup(dest_dir->children, dest_file)) {
    g_free(orig_file);
    g_free(dest_file);
    debug("ENOENT\n");
    return -ENOENT;
  }

  // extract the item (and it's key, since we'll need to delete that later) from
  // orig.
  gpointer old_name, item;
  if(!g_hash_table_lookup_extended(orig_dir->children, orig_file, &old_name, &item)) {
    debug("ENOENT\n");
    g_free(orig_file);
    g_free(dest_file);
  }

  // steal the item — meaning that it gets deleted from orig_dir, but not dereferenced.
  assert(g_hash_table_steal(orig_dir->children, orig_file));
  g_free(old_name);
  g_free(orig_file);

  // insert item into dest_dir.
  assert(g_hash_table_insert(dest_dir->children, dest_file, item));
  debug("success.\n");
  */

  return 0;
}

static int
ramdisk_symlink(const char *dest, const char *path)
{
/*
  debug("symlink(%s, %s) = ", dest, path);
  struct fuse_context *ctx;
  ramdisk_fs *fs;
  ramdisk_node *node;
  gpointer filename;
  ramdisk_get_ctx_fs_node(ctx, fs, node, path, &filename);
  ramdisk_node *node = ramdisk_node_get(NULL, path, NULL);

  // make sure the parent dir or dest exists.
  if(!ramdisk_is_dir(node)) {
    g_free(filename);
    debug("ENOENT\n");
    return -ENOENT;
  }

  // make sure that dest doesn't already exist.
  if(g_hash_table_lookup(node->children, filename)) {
    g_free(filename);
    debug("ENOENT\n");
    return -ENOENT;
  }

  // insert item into dest_dir.
  debug("success.\n");
  ramdisk_node *link = ramdisk_node_link(NULL, g_strdup(dest));
  assert(g_hash_table_insert(node->children, filename, link));
  */

  return 0;
}

static int
ramdisk_readlink(const char *path, char *dest, size_t size)
{
/*
  debug("readlink(%s) = ", path);
  struct fuse_context *ctx;
  ramdisk_fs *fs;
  ramdisk_node *node;
  ramdisk_get_ctx_fs_node(ctx, fs, node, path, NULL);
  ramdisk_node *node = ramdisk_node_get(NULL, path, NULL);

  if(!ramdisk_is_link(node)) {
    debug("ENOENT\n");
    return -ENOENT;
  }

  size = (size > node->size) ? node->size : size - 1;
  memcpy(dest, node->target, size);
  dest[size] = '\0';
  debug("%s\n", node->target);
  */

  return 0;
}

static int
ramdisk_link(const char *dest, const char *path)
{
/*
  debug("link(%s, %s) = ", path, dest);
  gpointer filename;
  ramdisk_node *node = ramdisk_node_parent(&fs->root, path, &filename);
  ramdisk_node *dest_node = ramdisk_node_parent(&fs->root, dest, NULL);

  // make sure the parent dirs or orig and dest exist.
  if(!dest_node || !ramdisk_is_dir(node)) {
    g_free(filename);
    debug("ENOENT\n");
    return -ENOENT;
  }

  // make sure that dest doesn't already exist.
  if(g_hash_table_lookup(node->children, filename)) {
    g_free(filename);
    debug("ENOENT\n");
    return -ENOENT;
  }

  // insert item into dest_dir.
  dest_node->nlink++;
  assert(g_hash_table_insert(node->children, filename, dest_node));
  debug("success.\n");
  */

  return 0;
}

static int
ramdisk_open(const char *path, struct fuse_file_info *fi)
{
  /*
  debug("open(%s) = ", path);
  ramdisk_node *node = ramdisk_node_parent(NULL, path, NULL);

  if(!ramdisk_is_file(node)) {
    debug("ENOENT\n");
    return -ENOENT;
  }

  debug("success.\n");
  */
  return 0;
}

static int
ramdisk_read(const char *path,
             char *out,
             size_t len,
             off_t pos,
             struct fuse_file_info *fi)
{
  /*
  debug("read(%s, %zu, %zu) = ", path, len, (size_t) pos);
  ramdisk_node *node = ramdisk_node_parent(NULL, path, NULL);

  if(!ramdisk_is_file(node)) {
    return -ENOENT;
  }

  GPtrArray *contents = node->contents;
  assert(contents);

  size_t max_len = node->size - pos;
  size_t read_len = max_len < len ? max_len : len;
  size_t first_block = pos / RAMDISK_BLOCKSIZE;
  size_t last_block = (pos + read_len - 1) / RAMDISK_BLOCKSIZE + 1;
  size_t copied = 0;
  for(size_t cur_block = first_block; cur_block < last_block; cur_block++) {
    const char *block = g_ptr_array_index(contents, cur_block);
    size_t block_offset = (pos + copied) % RAMDISK_BLOCKSIZE;
    size_t block_space = RAMDISK_BLOCKSIZE - block_offset;
    size_t remaining = read_len - copied;
    size_t copy_len = remaining < block_space ? remaining : block_space;
    assert(block);

    memcpy(&out[copied], &block[block_offset], copy_len);
    copied += copy_len;
  }

  return read_len;
  */
  return 0;
}

static int
ramdisk_write(const char *path,
              const char *data,
              size_t len,
              off_t pos,
              struct fuse_file_info *fi)
{
  /*
  debug("write(%s, %zu, %lld)\n", path, len, pos);
  debug("contents = '%s'\n", data);
  ramdisk_node *node = ramdisk_node_parent(NULL, path, NULL);

  if(!ramdisk_is_file(node)) {
    debug("ENOENT\n");
    return -ENOENT;
  }

  GPtrArray *contents = node->contents;
  assert(contents);

  if(len == 0) return 0;

  int bytes_to_allocate = (pos + len) - (contents->len * RAMDISK_BLOCKSIZE);
  if(0 < bytes_to_allocate) {
    size_t blocks_to_allocate = (bytes_to_allocate + RAMDISK_BLOCKSIZE - 1) / RAMDISK_BLOCKSIZE;
    for(size_t cur_insert; cur_insert < blocks_to_allocate; cur_insert++) {
      g_ptr_array_add(contents, malloc(RAMDISK_BLOCKSIZE));
    }

    node->size += bytes_to_allocate;
  }

  size_t first_block = pos / RAMDISK_BLOCKSIZE;
  size_t last_block = (pos + len - 1) / RAMDISK_BLOCKSIZE + 1;
  size_t copied = 0;
  for(size_t cur_block = first_block; cur_block < last_block; cur_block++) {
    char *block = g_ptr_array_index(contents, cur_block);
    size_t block_offset = (pos + copied) % RAMDISK_BLOCKSIZE;
    size_t block_space = RAMDISK_BLOCKSIZE - block_offset;
    size_t remaining = len - copied;
    size_t copy_len = remaining < block_space ? remaining : block_space;
    assert(block);

    memcpy(&block[block_offset], &data[copied], copy_len);
    copied += copy_len;
  }

  return len;
  */

  return 0;
}

static int
ramdisk_truncate(const char *path, off_t size) {
  /*
  debug("truncate(%s, %lld)\n", path, size);
  ramdisk_node *node = ramdisk_node_parent(NULL, path, NULL);
  
  assert(0 <= size);

  if(!ramdisk_is_file(node)) {
    return -ENOENT;
  }

  GPtrArray *contents = node->contents;
  assert(contents);

  int added_size = size - node->size;
  if(0 < added_size) {
    char *last_block = g_ptr_array_index(contents, contents->len - 1);
    size_t last_block_len = node->size % RAMDISK_BLOCKSIZE;
    memset(&last_block[last_block_len], 0, RAMDISK_BLOCKSIZE - last_block_len);

    size_t new_block_count = (size + RAMDISK_BLOCKSIZE - 1) / RAMDISK_BLOCKSIZE;
    for(size_t cur_block = node->size; cur_block < new_block_count; cur_block++) {
      g_ptr_array_add(contents, calloc(1, RAMDISK_BLOCKSIZE));
    }

  } else {
    size_t new_block_count = (size + RAMDISK_BLOCKSIZE - 1) / RAMDISK_BLOCKSIZE;
    g_ptr_array_set_size(contents, new_block_count);
  }

  node->size += added_size;
  */
  return 0;
}
static struct fuse_operations op = {
  .init    = ramdisk_init,
  .getattr = ramdisk_getattr,
  .readdir = ramdisk_readdir,
  //.create  = ramdisk_create,
  //.mkdir   = ramdisk_mkdir,
  //.rmdir   = ramdisk_rmdir,
  //.unlink  = ramdisk_unlink,
  //.rename  = ramdisk_rename,
  //.symlink = ramdisk_symlink,
  //.readlink= ramdisk_readlink,
  //.link    = ramdisk_link,
  //.open    = ramdisk_open,
  //.write   = ramdisk_write,
  //.read    = ramdisk_read,
  //.truncate= ramdisk_truncate,
};

int main(int argc, char *argv[])
{
  return fuse_main(argc, argv, &op, NULL);
}
